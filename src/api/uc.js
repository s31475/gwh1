import request from '@/utils/request2'
import qs from 'qs'
import { Login, userInfo, GET_RESOURCE3, SERVICE_ADMIN } from './api'

export function getUser(data) {
  return request({
    headers: {
      'Content-Type': 'application/x-www-form-urlencoded'
    },
    url: SERVICE_ADMIN + '/uc/api/user/page/list',
    method: 'post',
    data: qs.stringify(data)
  })
}

/**
 * start 资源相关的======================================
 */

export function getResource(roleCode) {
  return request({
    headers: {
      'Content-Type': 'application/x-www-form-urlencoded'
    },
    url: SERVICE_ADMIN + '/sys/menu/v2/resource/' + roleCode,
    method: 'post'
  })
}
export function getResource3(roleCode) {
  return request({
    headers: {
      'Content-Type': 'application/x-www-form-urlencoded'
    },
    url: GET_RESOURCE3 + '?roleCode=' + roleCode,
    method: 'GET'
  })
}

/**
 * 更新菜单
 * @param roleCode
 * @param resourceIds
 */
export function updateMenu(data) {
  return request({
    headers: {
      'Content-Type': 'application/x-www-form-urlencoded'
    },
    url: SERVICE_ADMIN + '/uc/api/role/updateMenu',
    method: 'post',
    data: qs.stringify(data)

  })
}

/**
 * end 资源相关的======================================
 */

/**
 * start人员相关的======================================
 */
/**
 * 保存用户信息
 * @param data
 */
export function saveUser(data) {
  return request({
    headers: {
      'Content-Type': 'application/json'
    },
    url: SERVICE_ADMIN + '/uc/api/user/v2/save',
    method: 'POST',
    data: JSON.stringify(data)
  })
}

/**
 * 更新密码
 * @param data
 */
export function updataPassword(data) {
  return request({
    headers: {
      'Content-Type': 'application/x-www-form-urlencoded'
    },
    url: SERVICE_ADMIN + '/sys/user/password',
    method: 'post',
    data: qs.stringify(data)
  })
}
export function getUserInfo(id) {
  return request({
    headers: {
      'Content-Type': 'application/json'
    },
    url: SERVICE_ADMIN + '/uc/api/user/info/' + id,
    method: 'GET'
  })
}

export function saveOrgUsers(data) {
  return request({
    headers: {
      'Content-Type': 'application/json'
    },
    url: SERVICE_ADMIN + '/uc/api/user/user/org/post/save',
    method: 'POST',
    data: JSON.stringify(data)
  })
}

/**
 * 删除用户
 * @param data
 */
export function removeUser(data) {
  return request({
    headers: {
      'Content-Type': 'application/json'
    },
    url: SERVICE_ADMIN + '/uc/api/user/users/remove',
    method: 'POST',
    data: data
  })
}

/**
 * 获取组织下面的岗位 包含子孙岗位
 * @param code
 */
export function getOrgPostAllList(code) {
  return request({
    headers: {
      'Content-Type': 'application/x-www-form-urlencoded'
    },
    url: SERVICE_ADMIN + '/uc/api/orgPost/list/code/all/' + code,
    method: 'post'
  })
}

/**
 * end 人员相关的======================================
 */

/**
 * 组织相关的======================================
 */

/**
 * 获取组织下面的岗位
 * @param data
 */
export function getOrgPostList(data) {
  return request({
    headers: {
      'Content-Type': 'application/x-www-form-urlencoded'
    },
    url: SERVICE_ADMIN + '/uc/api/orgPost/list/page/code/' + data.code,
    method: 'post',
    data: qs.stringify(data)
  })
}

/**
 * 设置为主组织
 * @param id
 */
export function setMaster(id) {
  return request({
    headers: {
      'Content-Type': 'application/x-www-form-urlencoded'
    },
    url: SERVICE_ADMIN + '/uc/api/orgUser/setMaster/' + id,
    method: 'GET'
  })
}

/**
 * 设置为主负责人
 * @param orgPostId
 * @param id
 */
export function setLeader(orgPostId, id) {
  return request({
    headers: {
      'Content-Type': 'application/x-www-form-urlencoded'
    },
    url: SERVICE_ADMIN + '/uc/api/orgUser/setLeader/' + orgPostId + '/2',
    method: 'GET'
  })
}

/**
 * 设置负责人
 * @param orgPostId
 * @param id
 */
export function setNormalLeader(orgPostId, id) {
  return request({
    headers: {
      'Content-Type': 'application/x-www-form-urlencoded'
    },
    url: SERVICE_ADMIN + '/uc/api/orgUser/setLeader/' + orgPostId + '/1',
    method: 'GET'
  })
}

/**
 * 取消负责人
 * @param orgPostId
 * @param id
 */
export function setCancleNormalLeader(orgPostId, id) {
  return request({
    headers: {
      'Content-Type': 'application/x-www-form-urlencoded'
    },
    url: SERVICE_ADMIN + '/uc/api/orgUser/setLeader/' + orgPostId + '/0',
    method: 'GET'
  })
}

/**
 * 下属人员
 * @param data
 */
export function getUserUnder(data) {
  return request({
    headers: {
      'Content-Type': 'application/x-www-form-urlencoded'
    },
    url: SERVICE_ADMIN + '/uc/api/userUnder/page/get/' + data.id + '/' + data.code,
    method: 'POST',
    data: qs.stringify(data)
  })
}

/**
 * 保存下属用户
 * @param data
 */
export function saveUnderUser(data) {
  return request({
    headers: {
      'Content-Type': 'application/json'
    },
    url: SERVICE_ADMIN + '/uc/api/userUnder/userOrgPost/save',
    method: 'POST',
    data: JSON.stringify(data)
  })
}

/**
 * 非当前用户的下属人员
 * @param data
 */
export function getUserNotUnderListAll(data) {
  return request({
    headers: {
      'Content-Type': 'application/x-www-form-urlencoded'
    },
    url: SERVICE_ADMIN + '/uc/api/user/page/listAll/under/' + data.id + '/' + data.code,
    method: 'POST',
    data: qs.stringify(data)
  })
}

/**
 * 删除下属用户
 * @param data
 */
export function removeUserUnder(data) {
  return request({
    headers: {
      'Content-Type': 'application/json'
    },
    url: SERVICE_ADMIN + '/uc/api/userUnder/v2/remove',
    method: 'POST',
    data: data
  })
}

/**
 * 取消主负责人
 * @param orgPostId
 * @param id
 */
export function setCancleLeader(orgPostId, id) {
  return request({
    headers: {
      'Content-Type': 'application/x-www-form-urlencoded'
    },
    url: SERVICE_ADMIN + '/uc/api/orgUser/setLeader/' + orgPostId + '/1',
    method: 'GET'
  })
}

/**
 * 删除组织下面的用户
 * @param data
 */
export function removeOrgUser(data) {
  return request({
    headers: {
      'Content-Type': 'application/json'
    },
    url: SERVICE_ADMIN + '/uc/api/orgUser/v2/remove',
    method: 'POST',
    data: data
  })
}

/**
 * 查询组织
 * @param id
 */
export function getOrgById(id) {
  return request({
    headers: {
      'Content-Type': 'application/x-www-form-urlencoded'
    },
    url: SERVICE_ADMIN + '/uc/api/org/get/' + id,
    method: 'GET'
  })
}

export function getOrgTableList(data) {
  return request({
    headers: {
      'Content-Type': 'application/x-www-form-urlencoded'
    },
    url: SERVICE_ADMIN + '/uc/api/org/tree/loadTable/' + data.code,
    method: 'post',
    data: qs.stringify(data)
  })
}
export function saveOrg(data) {
  return request({
    headers: {
      'Content-Type': 'application/json'
    },
    url: SERVICE_ADMIN + '/uc/api/org/save',
    method: 'POST',
    data: JSON.stringify(data)
  })
}

/**
 * 删除组织
 * @param data
 */
export function removeOrg(data) {
  return request({
    headers: {
      'Content-Type': 'application/json'
    },
    url: SERVICE_ADMIN + '/uc/api/org/v2/remove',
    method: 'POST',
    data: data
  })
}

/**
 * 组织相关的======================================
 */

export function getRoleTree(data) {
  return request({
    headers: {
      'Content-Type': 'application/x-www-form-urlencoded'
    },
    url: SERVICE_ADMIN + '/uc/api/role/list/all',
    method: 'post',
    data: qs.stringify(data)
  })
}
export function getDemension(data) {
  return request({
    headers: {
      'Content-Type': 'application/x-www-form-urlencoded'
    },
    url: SERVICE_ADMIN + '/uc/api/demension/list',
    method: 'post',
    data: qs.stringify(data)
  })
}
export function getOrgTree(code) {
  return request({
    headers: {
      'Content-Type': 'application/x-www-form-urlencoded'
    },
    url: SERVICE_ADMIN + '/uc/api/org/tree/loadTree/' + code,
    method: 'post'
  })
}
export function getRoleFilter(data, code) {
  return request({
    headers: {
      'Content-Type': 'application/x-www-form-urlencoded'
    },
    url: SERVICE_ADMIN + '/uc/api/role/user/get/filter/' + code,
    method: 'post',
    data: qs.stringify(data)
  })
}
export function getOrgFilter(data, code) {
  return request({
    headers: {
      'Content-Type': 'application/x-www-form-urlencoded'
    },
    url: SERVICE_ADMIN + '/uc/api/user/page/get/org/filter/' + code,
    method: 'post',
    data: qs.stringify(data)
  })
}
export function getPostFilter(data, code) {
  return request({
    headers: {
      'Content-Type': 'application/x-www-form-urlencoded'
    },
    url: SERVICE_ADMIN + '/uc/api/user/page/get/post/filter/' + code,
    method: 'post',
    data: qs.stringify(data)
  })
}

export function getOrgPost(data, code) {
  return request({
    headers: {
      'Content-Type': 'application/x-www-form-urlencoded'
    },
    url: SERVICE_ADMIN + '/uc/api/orgPost/list/all',
    method: 'GET'
  })
}
export function getAllUser(data, code) {
  return request({
    headers: {
      'Content-Type': 'application/x-www-form-urlencoded'
    },
    url: SERVICE_ADMIN + '/uc/api/user/page/listAll',
    method: 'POST',
    data: qs.stringify(data)
  })
}

/**
 * 获取角色管理
 * @param data
 */
export function getAllRole(data) {
  return request({
    headers: {
      'Content-Type': 'application/x-www-form-urlencoded'
    },
    url: SERVICE_ADMIN + '/uc/api/role/list/page',
    method: 'POST',
    data: qs.stringify(data)
  })
}

/**
 * 查询所有维度
 * @param data
 */
export function getDemensionList(data) {
  return request({
    headers: {
      'Content-Type': 'application/x-www-form-urlencoded'
    },
    url: SERVICE_ADMIN + '/uc/api/demension/list',
    method: 'POST',
    data: qs.stringify(data)
  })
}

/**
 * 删除岗位
 * @param data
 */
export function removeOrgPost(data) {
  return request({
    headers: {
      'Content-Type': 'application/json'
    },
    url: SERVICE_ADMIN + '/uc/api/orgPost/v2/remove',
    method: 'POST',
    data: data
  })
}

/**
 * 获取岗位+编码的所有用户
 * @param data
 */
export function getOrgPostByCodeAndOrgId(data) {
  return request({
    headers: {
      'Content-Type': 'application/x-www-form-urlencoded'
    },
    url: SERVICE_ADMIN + '/uc/api/user/page/get/post/v2/',
    method: 'POST',
    data: qs.stringify(data)

  })
}

/**
 * 维度分类
 */
export function getDemensionTypesByKey(code) {
  return request({
    headers: {
      'Content-Type': 'application/json'
    },
    url: SERVICE_ADMIN + '/uc/api/org/tree/loadTree/' + code,
    method: 'get'
  })
}
export function saveRoleUnderOrgCodeUser(data) {
  return request({
    headers: {
      'Content-Type': 'application/json'
    },
    url: SERVICE_ADMIN + '/uc/api/user/userOrgPost/save',
    method: 'POST',
    data: JSON.stringify(data)
  })
}

/**
 * 保存维度
 * @param data
 */
export function saveDemension(data) {
  return request({
    headers: {
      'Content-Type': 'application/json'
    },
    url: SERVICE_ADMIN + '/uc/api/demension/save',
    method: 'POST',
    data: JSON.stringify(data)
  })
}

/**
 * 保存岗位
 * @param data
 */
export function saveOrgPost(data) {
  return request({
    headers: {
      'Content-Type': 'application/json'
    },
    url: SERVICE_ADMIN + '/uc/api/orgPost/save',
    method: 'POST',
    data: JSON.stringify(data)
  })
}
export function getDemensionById(id) {
  return request({
    headers: {
      'Content-Type': 'application/json'
    },
    url: SERVICE_ADMIN + '/uc/api/demension/get/' + id,
    method: 'GET'
  })
}
/**
 * 删除维度
 * @param data
 */
export function removeDemension(data) {
  return request({
    headers: {
      'Content-Type': 'application/json'
    },
    url: SERVICE_ADMIN + '/uc/api/demension/v2/remove',
    method: 'POST',
    data: data
  })
}
export function setDemensionDefault(id) {
  return request({
    headers: {
      'Content-Type': 'application/json'
    },
    url: SERVICE_ADMIN + '/uc/api/demension/default/' + id,
    method: 'GET'
  })
}

/**
 * 职务相关的======================================
 */
/**
 * 删除职务
 * @param data
 */
export function removeJob(data) {
  return request({
    headers: {
      'Content-Type': 'application/json'
    },
    url: SERVICE_ADMIN + '/uc/api/job/v2/remove',
    method: 'POST',
    data: data
  })
}

/**
 * 查询所有的职务
 * @param data
 */
export function getJobList(data) {
  return request({
    headers: {
      'Content-Type': 'application/x-www-form-urlencoded'
    },
    url: SERVICE_ADMIN + '/uc/api/job/list',
    method: 'POST',
    data: qs.stringify(data)
  })
}

/**
 * 查询特定职务
 * @param data
 */
export function getJobListByDemension(data) {
  return request({
    headers: {
      'Content-Type': 'application/x-www-form-urlencoded'
    },
    url: SERVICE_ADMIN + '/uc/api/orgPost/list/' + data.code,
    method: 'POST',
    data: qs.stringify(data)
  })
}

/**
 * 根据path 查找岗位
 * @param data
 */
export function getJobListByDemensionPath(data) {
  return request({
    headers: {
      'Content-Type': 'application/x-www-form-urlencoded'
    },
    url: SERVICE_ADMIN + '/uc/api/orgPost/list/' + data.path,
    method: 'POST',
    data: qs.stringify(data)
  })
}

/**
 * 查询执行的岗位
 * @param id
 */
export function getOrgPostById(id) {
  return request({
    headers: {
      'Content-Type': 'application/x-www-form-urlencoded'
    },
    url: SERVICE_ADMIN + '/uc/api/orgPost/get/' + id,
    method: 'GET'
  })
}

/**
 * 组织下属人员管理
 * @param data
 */
export function getByOrgUser(data) {
  return request({
    headers: {
      'Content-Type': 'application/x-www-form-urlencoded'
    },
    url: SERVICE_ADMIN + '/uc/api/user/page/getByOrg/' + data.code,
    method: 'POST',
    data: qs.stringify(data)
  })
}

/**
 * 保存职务
 * @param data
 */
export function saveJob(data) {
  return request({
    headers: {
      'Content-Type': 'application/json'
    },
    url: SERVICE_ADMIN + '/uc/api/job/save',
    method: 'POST',
    data: JSON.stringify(data)
  })
}

/**
 * 根据id获取职务
 * @param id
 */
export function getJobById(id) {
  return request({
    headers: {
      'Content-Type': 'application/json'
    },
    url: SERVICE_ADMIN + '/uc/api/job/get/' + id,
    method: 'get'
  })
}
/**
 * 职务相关的======================================
 */

/**
 * 角色相关的======================================
 */

/**
 * 保存角色
 * @param data
 */
export function roleSave(data) {
  return request({
    headers: {
      'Content-Type': 'application/json'
    },
    url: SERVICE_ADMIN + '/uc/api/role/save',
    method: 'POST',
    data: JSON.stringify(data)
  })
}
export function roleUnderUser(data) {
  return request({
    headers: {
      'Content-Type': 'application/x-www-form-urlencoded'
    },
    url: SERVICE_ADMIN + '/uc/api/role/page/user/get/' + data.code,
    method: 'POST',
    data: qs.stringify(data)
  })
}

/**
 * 不在当前角色下面的用户
 * @param data
 */
export function getRoleNotUnderUser(data) {
  return request({
    headers: {
      'Content-Type': 'application/x-www-form-urlencoded'
    },
    url: SERVICE_ADMIN + '/uc/api/role/page/user/common/user/notUnder/' + data.code,
    method: 'POST',
    data: qs.stringify(data)
  })
}

/**
 * 角色与用户绑定
 * @param data
 */
export function saveRoleUnderUser(data) {
  return request({
    headers: {
      'Content-Type': 'application/json'
    },
    url: SERVICE_ADMIN + '/uc/api/role/userRole/save',
    method: 'POST',
    data: JSON.stringify(data)
  })
}

/**
 * 更新角色
 * @param data
 */
export function updateRole(data) {
  return request({
    headers: {
      'Content-Type': 'application/json'
    },
    url: SERVICE_ADMIN + '/uc/api/role/update',
    method: 'POST',
    data: JSON.stringify(data)
  })
}

/**
 * 获取角色
 * @param id
 */
export function getRole(id) {
  return request({
    headers: {
      'Content-Type': 'application/json'
    },
    url: SERVICE_ADMIN + '/uc/api/role/get/' + id,
    method: 'GET'
  })
}

/**
 * 删除角色
 * @param data
 */
export function removeRole(data) {
  return request({
    headers: {
      'Content-Type': 'application/json'
    },
    url: SERVICE_ADMIN + '/uc/api/role/v2/remove',
    method: 'POST',
    data: data
  })
}
/**
 * 删除角色下面的用户
 * @param data
 */
export function removeUserUnderRole(data) {
  return request({
    headers: {
      'Content-Type': 'application/json'
    },
    url: SERVICE_ADMIN + '/uc/api/role/under/user/remove',
    method: 'POST',
    data: data
  })
}

/**
 * 删除组织下面的用户
 * @param data
 */
export function removeUserUnderOrgPost(data) {
  return request({
    headers: {
      'Content-Type': 'application/json'
    },
    url: SERVICE_ADMIN + '/uc/api/user/v2/remove',
    method: 'POST',
    data: data
  })
}

