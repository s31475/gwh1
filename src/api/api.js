export const SERVICE_ADMIN = '/pangu-uc'
export const SERVICE_FORM = '/pangu-form'
export const SERVICE_BPM = '/pangu-admin'

export const Login = SERVICE_ADMIN + '/uc/api/sys/getJwt'
export const userInfo = SERVICE_ADMIN + '/uc/api/sys/jwt/user/info'
export const GET_RESOURCE3 = SERVICE_ADMIN + '/sys/menu/v3/resource'

