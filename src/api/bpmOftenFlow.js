import request from '@/utils/request2'
import qs from 'qs'
import { Login, userInfo, GET_RESOURCE3, SERVICE_ADMIN, SERVICE_FORM, SERVICE_BPM } from './api'

/**
 * 加载数据
 * @param data
 */
export function loadWfModule(data) {
  return request({
    headers: {
      'Content-Type': 'application/x-www-form-urlencoded'
    },
    url: SERVICE_BPM + '/a1bpmn/api/menu/menu/page/loadWfModule',
    method: 'POST',
    data: qs.stringify(data)
  })
}

/**
 * 加载流程
 */
export function loadProcess(data) {
  return request({
    headers: {
      'Content-Type': 'application/x-www-form-urlencoded'
    },
    url: SERVICE_BPM + '/a1bpmn/api/models/listJson',
    method: 'POST',
    data: qs.stringify(data)
  })
}
export function saveLoadProcess(data) {
  return request({
    headers: {
      'Content-Type': 'application/x-www-form-urlencoded'
    },
    url: SERVICE_BPM + '/a1bpmn/api/menu/menu/response/save',
    method: 'POST',
    data: qs.stringify(data)
  })
}
/**
 * 获取指定的常用流程
 */
export function getWfModuleById(id) {
  return request({
    headers: {
      'Content-Type': 'application/x-www-form-urlencoded'
    },
    url: SERVICE_BPM + '/a1bpmn/api/menu/get/' + id,
    method: 'post'
  })
}

/**
 * 删除指定的常用流程
 * @param data
 */
export function removeWfModule(data) {
  return request({
    headers: {
      'Content-Type': 'application/json'
    },
    url: SERVICE_BPM + '/a1bpmn/api/menu/v2/remove',
    method: 'POST',
    data: data
  })
}
