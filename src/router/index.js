import Vue from 'vue'
import Router from 'vue-router'

Vue.use(Router)

/* Layout */
import Layout from '@/layout'

/* Router Modules */
import componentsRouter from './modules/components'
import chartsRouter from './modules/charts'
import tableRouter from './modules/table'
import nestedRouter from './modules/nested'

export const constantRoutes = [
  {
    path: '/',
    component: Layout,
    redirect: '/dashboard',
    children: [
      {
        path: 'dashboard',
        component: () => import('@/views/dashboard/index'),
        name: 'Dashboard',
        meta: { title: '首页', icon: 'dashboard', affix: true }
      }
    ]
  },
  {
    path: '/message',
    component: Layout,
    meta: { title: '信息管理', icon: 'documentation', affix: true },
    children: [
      {
        path: 'news',
        component: () => import('@/views/message/news/index'),
        meta: { title: '新闻资讯', icon: '' }
      },
      {
        path: 'notice',
        component: () => import('@/views/message/notice/index'),
        meta: { title: '公告推送', icon: '' }
      },
      {
        path: 'policy',
        component: () => import('@/views/message/policy/index'),
        meta: { title: '政策推广', icon: '' }
      },
      {
        path: 'approve',
        component: () => import('@/views/message/approve/index'),
        meta: { title: '信息审批', icon: '' }
      },
      {
        path: 'post',
        hidden: true,
        component: () => import('@/views/message/post'),
        meta: { title: '新建发布', icon: '' }
      },
      {
        path: 'draft',
        component: () => import('@/views/message/draft'),
        meta: { title: '草稿栏', icon: '' }
      }
    ]
  },
  {
    path: '/empty_house',
    component: Layout,
    meta: { title: '空置管理', icon: 'documentation', affix: true },
    children: [
      {
        path: 'garden',
        component: () => import('@/views/empty-house/garden/index'),
        meta: { title: '园区信息登记', icon: '' }
      },
      {
        path: 'garden_action',
        hidden: true,
        meta: { title: '园区信息登记', icon: '' },
        component: () => import('@/views/empty-house/garden/layout'),
        children: [
          {
            path: 'new_log',
            component: () => import('@/views/empty-house/garden/log'),
            meta: {
              title: '新建登记',
              icon: ''
            }
          },
          {
            path: 'edit',
            component: () => import('@/views/empty-house/garden/edit'),
            meta: {
              title: '园区信息修改',
              icon: ''
            }
          },
          {
            path: 'detail',
            component: () => import('@/views/empty-house/garden/detail'),
            meta: {
              title: '园区信息详情',
              icon: ''
            }
          }
        ]
      },
      {
        path: 'house',
        component: () => import('@/views/empty-house/house/index'),
        meta: { title: '房屋管理', icon: '' }
      },
      {
        path: 'house_action',
        hidden: true,
        meta: { title: '园区信息登记', icon: '' },
        component: () => import('@/views/empty-house/garden/layout'),
        children: [
          {
            path: 'new_log',
            component: () => import('@/views/empty-house/house/log'),
            meta: {
              title: '新建登记',
              icon: ''
            }
          },
          {
            path: 'edit',
            component: () => import('@/views/empty-house/garden/edit'),
            meta: {
              title: '园区信息修改',
              icon: ''
            }
          },
          {
            path: 'detail',
            component: () => import('@/views/empty-house/garden/detail'),
            meta: {
              title: '园区信息详情',
              icon: ''
            }
          }
        ]
      },
      {
        path: 'customer',
        component: () => import('@/views/empty-house/customer/index'),
        meta: { title: '目标客户管理', icon: '' }
      },
      {
        path: 'customer_action',
        hidden: true,
        meta: { title: '园区信息登记', icon: '' },
        component: () => import('@/views/empty-house/customer/layout'),
        children: [
          {
            path: 'new_log',
            component: () => import('@/views/empty-house/customer/log'),
            meta: {
              title: '新建登记',
              icon: ''
            }
          },
          {
            path: 'edit',
            component: () => import('@/views/empty-house/garden/edit'),
            meta: {
              title: '园区信息修改',
              icon: ''
            }
          },
          {
            path: 'detail',
            component: () => import('@/views/empty-house/garden/detail'),
            meta: {
              title: '园区信息详情',
              icon: ''
            }
          }
        ]
      }
    ]
  },
  {
    path: '/brooder',
    component: Layout,
    meta: {
      title: '孵化器',
      icon: 'documentation',
      affix: true
    },
    children: [
      {
        path: 'join',
        component: () => import('@/views/brooder/join/index'),
        meta: { title: '入孵申请', icon: '' }
      },

      {
        path: 'join_action',
        meta: { title: '园区信息登记', icon: '' },
        component: () => import('@/views/brooder/join/layout'),
        children: [
          {
            path: 'new_log',
            component: () => import('@/views/brooder/join/log'),
            meta: {
              title: '新建登记',
              icon: ''
            }
          }
          // {
          //   path: 'edit',
          //   component: () => import('@/views/brooder/join/edit'),
          //   meta: {
          //     title: '园区信息修改',
          //     icon: ''
          //   }
          // },
          // {
          //   path: 'detail',
          //   component: () => import('@/views/brooder/join/detail'),
          //   meta: {
          //     title: '园区信息详情',
          //     icon: ''
          //   }
          // },
        ]
      },
      {
        path: 'graduate',
        component: () => import('@/views/brooder/graduate/index'),
        meta: { title: '毕业申请', icon: '' }
      },
      {
        path: 'exit',
        component: () => import('@/views/brooder/exit/index'),
        meta: { title: '退出申请', icon: '' }
      },
      {
        path: 'office_approve',
        component: () => import('@/views/brooder/officeApprove/index'),
        meta: { title: '事务审核', icon: '' }
      }
    ]
  },

  {
    path: '/uc',
    name: '用户中心',
    component: Layout,
    sort: 4,
    redirect: '/uc/org/user',
    meta: { title: '组织管理', icon: '', noCache: true },
    children: [
      {
        path: '/uc/org/user',
        component: () => import('@/views/uc/user/index'),
        name: 'user_index',
        meta: { title: '人员管理', icon: '', noCache: true }
      },
      {
        path: '/uc/user/add',
        component: () => import('@/views/uc/user/add'),
        name: 'user_add',
        hidden: true,
        meta: { title: '人员信息', icon: '', noCache: true }
      },
      {
        path: '/uc/role/index',
        component: () => import('@/views/uc/role/index'),
        name: 'role_index',
        meta: { title: '角色管理', icon: '', noCache: true }
      },
      {
        path: '/uc/role/add',
        component: () => import('@/views/uc/role/add'),
        name: 'role_add',
        hidden: true,
        meta: { title: '角色管理', icon: '', noCache: true }
      },
      {
        path: '/uc/role/userList',
        component: () => import('@/views/uc/role/userList'),
        name: 'role_userList',
        hidden: true,
        meta: { title: '角色人员管理', icon: '', noCache: true }
      },
      {
        path: '/uc/ucorgjob/index',
        component: () => import('@/views/uc/ucorgjob/index'),
        name: 'ucorgjob_index',
        meta: { title: '职务管理', icon: '', noCache: true }
      },
      {
        path: '/uc/ucorgjob/add',
        component: () => import('@/views/uc/ucorgjob/add'),
        name: 'ucorgjob_add',
        hidden: true,
        meta: { title: '职务管理', icon: '', noCache: true }
      },
      {
        path: '/uc/demension/index',
        component: () => import('@/views/uc/demension/index'),
        name: 'demension_index',
        meta: { title: '组织维度管理', icon: '', noCache: true }
      },
      {
        path: '/uc/demension/add',
        component: () => import('@/views/uc/demension/add'),
        name: 'demension_add',
        hidden: true,
        meta: { title: '组织维度管理', icon: '', noCache: true }
      },
      {
        path: '/uc/ucorgpost/index',
        component: () => import('@/views/uc/ucorgpost/index'),
        name: 'ucorgpost_index',
        meta: { title: '岗位管理', icon: '', noCache: true }
      },
      {
        path: '/uc/ucorgpost/add',
        component: () => import('@/views/uc/ucorgpost/add'),
        name: 'ucorgpost_add',
        hidden: true,
        meta: { title: '岗位管理', icon: '', noCache: true }
      },
      {
        path: '/uc/ucorgpost/userList',
        component: () => import('@/views/uc/ucorgpost/userList'),
        name: 'ucorgpost_userList',
        hidden: true,
        meta: { title: '岗位人员管理', icon: '', noCache: true }
      },
      {
        path: '/uc/org/index',
        component: () => import('@/views/uc/org/index'),
        name: 'org_index',
        meta: { title: '组织管理', icon: '', noCache: true }
      },
      {
        path: '/uc/org/add',
        component: () => import('@/views/uc/org/add'),
        name: 'org_add',
        hidden: true,
        meta: { title: '组织管理', icon: '', noCache: true }
      },
      {
        path: '/uc/org/orgPost',
        component: () => import('@/views/uc/org/orgPost'),
        name: 'org_orgPost',
        hidden: true,
        meta: { title: '组织下属岗位', icon: '', noCache: true }
      },
      {
        path: '/uc/org/addUser',
        component: () => import('@/views/uc/org/addUser'),
        name: 'org_addUser',
        hidden: true,
        meta: { title: '添加人员', icon: '', noCache: true }
      },
      {
        path: '/uc/org/userList',
        component: () => import('@/views/uc/org/userList'),
        name: 'org_userList',
        hidden: true,
        meta: { title: '添加下属人员', icon: '', noCache: true }
      },
      {
        path: '/uc/org/addOrgPostUser',
        component: () => import('@/views/uc/org/addOrgPostUser'),
        name: 'org_addOrgPostUser',
        hidden: true,
        meta: { title: '组织添加人员', icon: '', noCache: true }
      }
    ],
    hidden: false
  },

  {
    path: '/form',
    name: '业务表单',
    component: Layout,
    sort: 2,
    redirect: '/formbus/businesstable/index',
    meta: { title: '业务表单', icon: '', noCache: true },
    children: [
      {
        path: '/formbus/design/app',
        component: () => import('@/views/formbus/design/app'),
        name: 'formModelerIndex',
        meta: { title: '表单设计器', icon: '', noCache: true },
        hidden: true
      },
      {
        path: '/formbus/design/modeler',
        component: () => import('@/views/formbus/design/modeler'),
        name: 'formModeler',
        meta: { title: '表单设计器', icon: '', noCache: true },
        hidden: true
      },
      {
        path: '/formbus/design/versionManager',
        component: () => import('@/views/formbus/design/versionManager'),
        name: 'formVersionManager',
        meta: { title: '版本管理', icon: '', noCache: true },
        hidden: true
      },
      {
        path: '/formbus/businesstable/index',
        component: () => import('@/views/formbus/businesstable/index'),
        name: 'businesstableIndex',
        meta: { title: '业务实体管理', icon: '', noCache: true }
      },
      {
        path: '/formbus/businesstable/inner/index',
        component: () => import('@/views/formbus/businesstable/inner/index'),
        name: 'businessTableInnerAdd',
        meta: { title: '新增业务实体', icon: '', noCache: true },
        hidden: true
      },
      {
        path: '/formbus/businessobject/index',
        component: () => import('@/views/formbus/businessobject/index'),
        name: 'businessObjIndex',
        meta: { title: '业务对象', icon: '', noCache: true }
      },
      {
        path: '/form/design/index',
        component: () => import('@/views/formbus/design/index'),
        name: 'designForm',
        meta: { title: '表单模型', icon: '', noCache: true }
      },
      {
        path: '/form/customDialog/index',
        component: () => import('@/views/formbus/customdialog/index'),
        name: 'customDialogList',
        meta: { title: '对话框管理', icon: '', noCache: true }
      },
      {
        path: '/formbus/businessobject/add',
        component: () => import('@/views/formbus/businessobject/add'),
        name: 'businessObjAdd',
        meta: { title: '新增业务对象', icon: '', noCache: true },
        hidden: true
      },
      {
        path: '/formbus/customdialog/add',
        component: () => import('@/views/formbus/customdialog/add'),
        name: 'customdialogAdd',
        meta: { title: '对话框', icon: '', noCache: true },
        hidden: true
      }
    ],
    hidden: false
  },
  {
    path: '/finance',
    component: Layout,
    sort: 7,
    redirect: '/finance/index',
    meta: { title: '流程辅助功能', icon: '', noCache: true },
    children: [
      {
        path: '/modelIndex',
        component: () => import('@/views/bpm/model/index'),
        name: 'Finance',
        meta: { title: '模型管理', icon: '', noCache: true }
      },
      {
        path: '/bpm/model/v4',
        component: () => import('@/views/bpm/model/v4'),
        name: 'v4Index',
        meta: { title: '业务流程设计器', icon: '', noCache: true }
      },
      {
        path: '/low/bpmOftenFlow/index',
        component: () => import('@/views/low/bpmOftenFlow/index'),
        name: 'bpmOftenFlowLowIndex',
        meta: { title: '业务平台常用流程', icon: '', noCache: true }
      },
      {
        path: '/testBpm',
        component: () => import('@/views/bpm/model/testBpm'),
        name: 'testBpm',
        meta: { title: '自动化流程测试', icon: '', noCache: true },
        hidden: true
      },
      {
        path: '/low/bpmOftenFlow/add',
        component: () => import('@/views/low/bpmOftenFlow/add'),
        name: 'addV4BpmOftenFlow',
        meta: { title: '添加常用流程', icon: '', noCache: true },
        hidden: true
      },
      {
        path: '/diffModel',
        component: () => import('@/views/bpm/model/diffModel'),
        name: 'testBpm',
        meta: { title: '自动化测试(BPS平台)', icon: '', noCache: true }
      },
      {
        path: 'busModel',
        component: () => import('@/views/bpm/model/busModel'),
        name: 'busModel',
        meta: { title: '模型业务设置', icon: '', noCache: true },
        hidden: true
      },
      {
        path: '/bpm/bpmOftenFlow/index',
        component: () => import('@/views/bpm/bpmOftenFlow/index'),
        name: 'bpmOftenFlow',
        meta: { title: '常用流程', icon: '', noCache: true }
      },
      {
        path: '/bpm/addBpmOftenFlow/index',
        component: () => import('@/views/bpm/bpmOftenFlow/add'),
        name: 'addBpmOftenFlow',
        meta: { title: '添加常用流程', icon: '', noCache: true },
        hidden: true
      },
      {
        path: '/bpm/model/versionManager',
        component: () => import('@/views/bpm/model/versionManager'),
        name: 'versionManager',
        meta: { title: '版本管理', icon: '', noCache: true },
        hidden: true
      },

      {
        path: '/bpm/runtime/instance/v1/getInstanceLis',
        component: () => import('@/views/bpm/taskinstance/index'),
        name: 'Finance',
        meta: { title: '实例管理', icon: '', noCache: true }
      },
      {
        path: '/bpm/his/instance/resurrection',
        component: () => import('@/views/bpm/resurrection/index'),
        name: 'resurrection_instance',
        meta: { title: '历史实例复活', icon: '', noCache: true }
      },
      {
        path: '/bpm/his/instance/setting',
        component: () => import('@/views/bpm/resurrection/setting'),
        name: 'resurrection_instance_setting',
        meta: { title: '实例复活设置', icon: '', noCache: true },
        hidden: true
      },
      {
        path: '/bpm/model/urging',
        component: () => import('@/views/bpm/model/urging'),
        name: 'urging_task_setting',
        meta: { title: '催办设置', icon: '', noCache: true },
        hidden: true
      },
      {
        path: '/bpm/model/expire',
        component: () => import('@/views/bpm/model/expire'),
        name: 'expire_task_setting',
        meta: { title: '到期提醒', icon: '', noCache: true },
        hidden: true
      },
      {
        path: '/bpm/model/sequenceModel',
        component: () => import('@/views/bpm/model/sequenceModel'),
        name: 'sequenceModel_setting',
        meta: { title: '连线按钮', icon: '', noCache: true },
        hidden: true
      },
      {
        path: '/bpm/messageTemplate/list',
        component: () => import('@/views/bpm/messageTemplate/index'),
        name: 'Finance',
        meta: { title: '流程消息模板', icon: '', noCache: true }
      },
      {
        path: '/bpm/script/index',
        component: () => import('@/views/bpm/script/index'),
        name: 'Finance',
        meta: { title: '常用脚本管理', icon: '', noCache: true }
      },
      {
        path: '/bpm/agent/index',
        component: () => import('@/views/bpm/agent/index'),
        name: 'Finance',
        meta: { title: '委托设置', icon: '', noCache: true }
      }
    ]
  },

  // {
  //   path: '/guide',
  //   component: Layout,
  //   redirect: '/guide/index',
  //   children: [
  //     {
  //       path: 'index',
  //       component: () => import('@/views/guide/index'),
  //       name: 'Guide',
  //       meta: { title: 'Guide', icon: 'guide', noCache: true }
  //     }
  //   ]
  // },
  // {
  //   path: '/profile',
  //   component: Layout,
  //   redirect: '/profile/index',
  //   hidden: true,
  //   children: [
  //     {
  //       path: 'index',
  //       component: () => import('@/views/profile/index'),
  //       name: 'Profile',
  //       meta: { title: 'Profile', icon: 'user', noCache: true }
  //     }
  //   ]
  // },
  {
    path: '/redirect',
    component: Layout,
    hidden: true,
    children: [
      {
        path: '/redirect/:path(.*)',
        component: () => import('@/views/redirect/index')
      }
    ]
  },
  {
    path: '/login',
    component: () => import('@/views/login/index'),
    hidden: true
  },
  {
    path: '/auth-redirect',
    component: () => import('@/views/login/auth-redirect'),
    hidden: true
  },
  {
    path: '/404',
    component: () => import('@/views/error-page/404'),
    hidden: true
  },
  {
    path: '/401',
    component: () => import('@/views/error-page/401'),
    hidden: true
  }
]

/**
 * asyncRoutes
 * the routes that need to be dynamically loaded based on user roles
 */
// export const asyncRoutes = [
//   {
//     path: '/permission',
//     component: Layout,
//     redirect: '/permission/page',
//     alwaysShow: true, // will always show the root menu
//     name: 'Permission',
//     meta: {
//       title: 'Permission',
//       icon: 'lock',
//       roles: ['admin', 'editor'] // you can set roles in root nav
//     },
//     children: [
//       {
//         path: 'page',
//         component: () => import('@/views/permission/page'),
//         name: 'PagePermission',
//         meta: {
//           title: 'Page Permission',
//           roles: ['admin'] // or you can only set roles in sub nav
//         }
//       },
//       {
//         path: 'directive',
//         component: () => import('@/views/permission/directive'),
//         name: 'DirectivePermission',
//         meta: {
//           title: 'Directive Permission'
//           // if do not set roles, means: this page does not require permission
//         }
//       },
//       {
//         path: 'role',
//         component: () => import('@/views/permission/role'),
//         name: 'RolePermission',
//         meta: {
//           title: 'Role Permission',
//           roles: ['admin']
//         }
//       }
//     ]
//   },

//   {
//     path: '/icon',
//     component: Layout,
//     children: [
//       {
//         path: 'index',
//         component: () => import('@/views/icons/index'),
//         name: 'Icons',
//         meta: { title: 'Icons', icon: 'icon', noCache: true }
//       }
//     ]
//   },

//   componentsRouter,
//   chartsRouter,
//   nestedRouter,
//   tableRouter,

//   {
//     path: '/example',
//     component: Layout,
//     redirect: '/example/list',
//     name: 'Example',
//     meta: {
//       title: 'Example',
//       icon: 'el-icon-s-help'
//     },
//     children: [
//       {
//         path: 'create',
//         component: () => import('@/views/example/create'),
//         name: 'CreateArticle',
//         meta: { title: 'Create Article', icon: 'edit' }
//       },
//       {
//         path: 'edit/:id(\\d+)',
//         component: () => import('@/views/example/edit'),
//         name: 'EditArticle',
//         meta: { title: 'Edit Article', noCache: true, activeMenu: '/example/list' },
//         hidden: true
//       },
//       {
//         path: 'list',
//         component: () => import('@/views/example/list'),
//         name: 'ArticleList',
//         meta: { title: 'Article List', icon: 'list' }
//       }
//     ]
//   },

//   {
//     path: '/tab',
//     component: Layout,
//     children: [
//       {
//         path: 'index',
//         component: () => import('@/views/tab/index'),
//         name: 'Tab',
//         meta: { title: 'Tab', icon: 'tab' }
//       }
//     ]
//   },

//   {
//     path: '/error',
//     component: Layout,
//     redirect: 'noRedirect',
//     name: 'ErrorPages',
//     meta: {
//       title: 'Error Pages',
//       icon: '404'
//     },
//     children: [
//       {
//         path: '401',
//         component: () => import('@/views/error-page/401'),
//         name: 'Page401',
//         meta: { title: '401', noCache: true }
//       },
//       {
//         path: '404',
//         component: () => import('@/views/error-page/404'),
//         name: 'Page404',
//         meta: { title: '404', noCache: true }
//       }
//     ]
//   },

//   {
//     path: '/error-log',
//     component: Layout,
//     children: [
//       {
//         path: 'log',
//         component: () => import('@/views/error-log/index'),
//         name: 'ErrorLog',
//         meta: { title: 'Error Log', icon: 'bug' }
//       }
//     ]
//   },

//   {
//     path: '/excel',
//     component: Layout,
//     redirect: '/excel/export-excel',
//     name: 'Excel',
//     meta: {
//       title: 'Excel',
//       icon: 'excel'
//     },
//     children: [
//       {
//         path: 'export-excel',
//         component: () => import('@/views/excel/export-excel'),
//         name: 'ExportExcel',
//         meta: { title: 'Export Excel' }
//       },
//       {
//         path: 'export-selected-excel',
//         component: () => import('@/views/excel/select-excel'),
//         name: 'SelectExcel',
//         meta: { title: 'Export Selected' }
//       },
//       {
//         path: 'export-merge-header',
//         component: () => import('@/views/excel/merge-header'),
//         name: 'MergeHeader',
//         meta: { title: 'Merge Header' }
//       },
//       {
//         path: 'upload-excel',
//         component: () => import('@/views/excel/upload-excel'),
//         name: 'UploadExcel',
//         meta: { title: 'Upload Excel' }
//       }
//     ]
//   },

//   {
//     path: '/zip',
//     component: Layout,
//     redirect: '/zip/download',
//     alwaysShow: true,
//     name: 'Zip',
//     meta: { title: 'Zip', icon: 'zip' },
//     children: [
//       {
//         path: 'download',
//         component: () => import('@/views/zip/index'),
//         name: 'ExportZip',
//         meta: { title: 'Export Zip' }
//       }
//     ]
//   },

//   {
//     path: '/pdf',
//     component: Layout,
//     redirect: '/pdf/index',
//     children: [
//       {
//         path: 'index',
//         component: () => import('@/views/pdf/index'),
//         name: 'PDF',
//         meta: { title: 'PDF', icon: 'pdf' }
//       }
//     ]
//   },
//   {
//     path: '/pdf/download',
//     component: () => import('@/views/pdf/download'),
//     hidden: true
//   },

//   {
//     path: '/theme',
//     component: Layout,
//     children: [
//       {
//         path: 'index',
//         component: () => import('@/views/theme/index'),
//         name: 'Theme',
//         meta: { title: 'Theme', icon: 'theme' }
//       }
//     ]
//   },

//   {
//     path: '/clipboard',
//     component: Layout,
//     children: [
//       {
//         path: 'index',
//         component: () => import('@/views/clipboard/index'),
//         name: 'ClipboardDemo',
//         meta: { title: 'Clipboard', icon: 'clipboard' }
//       }
//     ]
//   },

//   {
//     path: 'external-link',
//     component: Layout,
//     children: [
//       {
//         path: 'https://github.com/PanJiaChen/vue-element-admin',
//         meta: { title: 'External Link', icon: 'link' }
//       }
//     ]
//   },

//   // 404 page must be placed at the end !!!
//   { path: '*', redirect: '/404', hidden: true }
// ]

export const asyncRoutes = [
  { path: '*', redirect: '/404', hidden: true }
]

const createRouter = () => new Router({
  // mode: 'history', // require service support
  scrollBehavior: () => ({ y: 0 }),
  routes: constantRoutes
})

const router = createRouter()

// Detail see: https://github.com/vuejs/vue-router/issues/1234#issuecomment-357941465
export function resetRouter() {
  const newRouter = createRouter()
  router.matcher = newRouter.matcher // reset router
}

export default router
